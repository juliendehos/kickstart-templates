# kickstart-templates

[kickstart](https://github.com/Keats/kickstart)

```
kickstart https://gitlab.com/juliendehos/kickstart-templates -s hs-simple
kickstart https://gitlab.com/juliendehos/kickstart-templates -s hs-full
kickstart https://gitlab.com/juliendehos/kickstart-templates -s cpp-simple
kickstart https://gitlab.com/juliendehos/kickstart-templates -s py-simple
kickstart https://gitlab.com/juliendehos/kickstart-templates -s py-cpp
```

