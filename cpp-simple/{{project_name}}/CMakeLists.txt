cmake_minimum_required( VERSION 3.0 )
project( {{project_name}} )
include_directories( include )

# library
add_library( {{project_name}}-lib
    src/{{project_name}}/{{project_name}}.cpp )

# executable
add_executable( {{project_name}}-app
  app/main.cpp )
target_link_libraries( {{project_name}}-app {{project_name}}-lib )

# install
install( TARGETS {{project_name}}-app DESTINATION bin )

# testing
add_executable( {{project_name}}-test
    test/{{project_name}}/{{project_name}}-test.cpp
    test/main.cpp )
target_link_libraries( {{project_name}}-test {{project_name}}-lib )
enable_testing()
add_test( NAME {{project_name}}-test COMMAND {{project_name}}-test )

