
from setuptools import setup

setup(
    name = '{{project_name}}',
    version = '0.1.0',
    packages=['{{project_name}}'],
    scripts = ['scripts/main.py']
    )

