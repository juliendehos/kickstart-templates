
from setuptools import setup, Extension

exts = [Extension('{{project_name}}_ext', ['cpp/{{project_name}}_ext.cpp'])]

setup(
    name = '{{project_name}}',
    version = '0.1.0',
    ext_modules = exts,
    packages=['{{project_name}}'],
    scripts = [ 'scripts/main.py' ]
    )
