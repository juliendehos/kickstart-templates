
int mul(int x, int y) {
  return x*y;
}

#include <pybind11/pybind11.h>

PYBIND11_MODULE({{project_name}}_ext, m) {
    m.def("mul", &mul);
}

