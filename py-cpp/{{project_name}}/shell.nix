with import <nixpkgs> {};

python3Packages.buildPythonPackage {
    name = "{{project_name}}";
    src = ./.;
    propagatedBuildInputs = [ 
      python3Packages.pybind11
    ];
  }

