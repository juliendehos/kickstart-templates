#import {{project_name}}.{{project_name}} as {{project_name}}
from {{project_name}}.{{project_name}} import mul2

if __name__ == '__main__':
    
    x = 21
    print(f'mul2({x}) = {mul2(x)}')

